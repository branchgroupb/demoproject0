package com.cugx.Demo;

import com.cugx.dao.UserDao;
import com.cugx.entity.User;
import com.cugx.service.LogService;
import com.cugx.service.impl.LogServiceImpl;

public class Demo1 {
    public static void main(String[] args) {
        User user1 = new User("ZhangSan", 20, 1.8);
        System.out.println(user1.toString());
        UserDao userDao = new UserDao();
        if (userDao.queryDb(user1)) {
            System.out.println(user1.getName() + "用户已注册");
        }
        LogService logService = new LogServiceImpl();
        logService.Login(user1);
        logService.Logout(user1);
    }
}
