package com.cugx.service.impl;

import com.cugx.entity.User;
import com.cugx.frame.Frame;
import com.cugx.service.LogService;

public class LogServiceImpl implements LogService {
    @Override
    public void Login(User user) {
        System.out.println(user.getName() + " 于" + Frame.getCurrentTime() + " 登录成功");
    }

    @Override
    public void Logout(User user) {
        System.out.println(user.getName() + " 于" + Frame.getCurrentTime() + " 注销成功");
    }
}
