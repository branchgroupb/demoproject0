package com.cugx.service;

import com.cugx.entity.User;

public interface LogService {
    void Login(User user);
    void Logout(User user);
}
