package com.cugx.frame;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Frame {
    public static String getCurrentTime() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        return simpleDateFormat.format(calendar.getTime());
    }
}
